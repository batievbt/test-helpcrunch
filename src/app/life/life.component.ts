import { Component } from '@angular/core';
import { Board } from '../models/board';

@Component({
  selector: 'app-life',
  templateUrl: './life.component.html',
  styleUrls: ['./life.component.css']
})

export class LifeComponent {
  _board: Board; // Board model
  _graphicalBoard: number[][]; // Board array
  _executionInterval: any; // Execution time interval

  // Constructor. Set graphical board as empty array before starting execution
  constructor() {
    this._graphicalBoard = [];
  }

  // Main function. It makes board updated according to the game's rules
  execute() {
    this._graphicalBoard  = this._board.updateBoardWithRules();
  }

  // Event for initializing board with given amount of rows and columns
  initializeBoard(rows: number, columns: number) {
    if (this._executionInterval) {
      this.stopExecution();
    }

    this._board = new Board(rows, columns);

    this._graphicalBoard = this._board.getBoard();
  }

  // Event for starting execution of the game
  startExecution() {
    this._executionInterval = setInterval(() => {
      this.execute();
    }, 2000);
  }

  // Event for stopping execution of the game
  stopExecution() {
    window.clearInterval(this._executionInterval);
  }

  // Reset execution. Create new board, stop previous time interval and run execution from the very beginning
  resetExecution() {
    if (this._executionInterval) {
      this.stopExecution();
    }

    const rows = this._board.getRows();
    const columns = this._board.getColumns();

    this._board = new Board(rows, columns);
    this._graphicalBoard = this._board.getBoard();

    this.startExecution();
  }
}
