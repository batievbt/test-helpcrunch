import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LifeComponent } from './life/life.component';


@NgModule({
  declarations: [
    LifeComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [LifeComponent]
})
export class AppModule { }
