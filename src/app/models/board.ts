export class Board {

    // Main board itself
    private _board: number[][];

    // Initialize amount of rows and column and build the board
    constructor(private _rows: number, private _columns: number) {

        this._board = this.buildBoard();
    }

    // Return the board
    getBoard() {
        return this._board;
    }

    // Get amount of rows
    getRows() {
        return this._rows;
    }

    // Get amount of columns
    getColumns() {
        return this._columns;
    }

    // Update the board according to the game's rules
    updateBoardWithRules() {
        // Initialize new board, it will be used for saving previous values of main board
        const newBoard = this.buildBoard();

        let totalAliveCells = 0;
        for (let x = 0; x < this.getRows(); x++) {
            for (let y = 0; y < this.getColumns(); y++) {
                // Calculate alive cells nearby current cell
                const aliveCells = this.calculateAliveNeighboringCells(x, y);
                totalAliveCells += aliveCells;

                switch (aliveCells) {
                    // If there are two alive neighboring cells - keep cell's value
                    case 2:
                        newBoard[x][y] = this._board[x][y];
                        break;

                    // If there are three alive cells nearby - set cell as alive
                    case 3:
                        newBoard[x][y] = 1;
                        break;

                    // By default - make cell dead
                    default:
                        newBoard[x][y] = 0;
                        break;
                }
            }
        }

        // Reinitialize board with updated values
        this._board = newBoard;

        return totalAliveCells > 0 ? this._board : [];
    }

    // Build board with random values
    private buildBoard() {
        const board: number[][] = [];

        for (let x = 0; x < this.getRows(); x++) {

            const rows = [];

            for (let y = 0; y < this.getColumns(); y++) {
                rows.push(Math.round(Math.random()));
            }

            board.push(rows);
        }

        return board;
    }

    // Calculate amount of alive neighboring cells for specific cell
    private calculateAliveNeighboringCells(row, column) {
        let aliveCells = 0;

        if (row > 0 && column > 0 && column < this.getColumns() - 1) {
            // Previous row
            aliveCells += this._board[row - 1][column - 1] ? 1 : 0;
            aliveCells += this._board[row - 1][column] ? 1 : 0;
            aliveCells += this._board[row - 1][column + 1] ? 1 : 0;
        }

        if (column > 0 && column < this.getColumns() - 1) {
            // Current row
            aliveCells += this._board[row][column - 1] ? 1 : 0;
            aliveCells += this._board[row][column + 1] ? 1 : 0;
        }

        if (row < this.getRows() - 1 && column > 0 && column < this.getColumns() - 1) {
            // Next row
            aliveCells += this._board[row + 1][column - 1] ? 1 : 0;
            aliveCells += this._board[row + 1][column] ? 1 : 0;
            aliveCells += this._board[row + 1][column + 1] ? 1 : 0;
        }

        return aliveCells;
    }
}
